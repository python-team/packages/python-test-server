--- a/tests/test_server.py
+++ b/tests/test_server.py
@@ -22,7 +22,44 @@
 )
 from test_server.server import INTERNAL_ERROR_RESPONSE_STATUS
 
-from .util import fixture_global_server, fixture_server  # pylint: disable=unused-import
+# file is missing from tarball
+#from .util import fixture_global_server, fixture_server  # pylint: disable=unused-import
+
+from collections.abc import Generator
+from threading import Lock
+
+import pytest
+from typing import TypedDict
+
+from test_server import TestServer
+
+
+class CacheDict(TypedDict, total=False):
+    server: TestServer
+
+
+CACHE: CacheDict = {}
+CACHE_LOCK = Lock()
+
+
+@pytest.fixture(scope="session", name="global_server")
+def fixture_global_server() -> Generator[TestServer, None, None]:
+    with CACHE_LOCK:
+        if "server" not in CACHE:
+            srv = TestServer()
+            srv.start()
+            CACHE["server"] = srv
+    yield CACHE["server"]
+    with CACHE_LOCK:
+        if CACHE["server"]:
+            CACHE["server"].stop()
+            del CACHE["server"]
+
+
+@pytest.fixture(name="server")
+def fixture_server(global_server: TestServer) -> TestServer:
+    global_server.reset()
+    return global_server
 
 NETWORK_TIMEOUT = 1
 SPECIFIC_TEST_PORT = 10100
